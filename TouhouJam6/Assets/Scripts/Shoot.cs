﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public GameObject shot_Prefab;
    public Transform shot_Parent;
    public List<ParticleSystem> shots_List = new List<ParticleSystem>();

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ShootFunc();
        }
    }

    public void ShootFunc()
    {
        for (int i = 0; i < shots_List.Count; i++)
        {
            if (!shots_List[i].isPlaying)
            {
                StartShot(shots_List[i]);
                shots_List[i].transform.position = GameManager.Instance.mousePos;
                shots_List[i].Play();
                return;
            }
        }
        GameObject newobj = Instantiate(shot_Prefab, shot_Parent);
        shots_List.Add(newobj.GetComponent<ParticleSystem>());
        StartShot(newobj.GetComponent<ParticleSystem>());
    }

    void StartShot(ParticleSystem _target)
    {
        StartCoroutine(DamageActivation());
        _target.transform.position = GameManager.Instance.mousePos;
        _target.Play();
    }

    IEnumerator DamageActivation()
    {
        transform.GetComponent<CircleCollider2D>().enabled = true;
        yield return new WaitForSeconds(0.5f);
        transform.GetComponent<CircleCollider2D>().enabled = false;
    }
}
