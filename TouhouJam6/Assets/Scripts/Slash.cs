﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slash : MonoBehaviour
{
    public GameObject Slash_Prefab;
    public Transform Slash_Parent;
    public List<ParticleSystem> Slash_List = new List<ParticleSystem>();
    ParticleSystem chosenSlash;
    public ParticleSystem trueSlash;

    public Vector3[] slashPath = new Vector3[2];

    private void Start()
    {
        slashPath = new Vector3[2];
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            slashPath[0] = GameManager.Instance.mousePos;
            ChooseSlash();
        }
        if (Input.GetMouseButton(0))
        {
            if (chosenSlash)
            {
               chosenSlash.transform.position = GameManager.Instance.mousePos;
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            slashPath[1] = GameManager.Instance.mousePos;
            StopSlash();
            if (Vector2.Distance(slashPath[0], slashPath[1]) > 3)
            {
                iTween.MoveTo(trueSlash.gameObject, iTween.Hash("path", slashPath, "movetopath",false, "time", 1f));
            }
        }
    }

    void ChooseSlash()
    {
        for (int i = 0; i < Slash_List.Count; i++)
        {
            if (!Slash_List[i].isPlaying)
            {
                chosenSlash = Slash_List[i];
                chosenSlash.transform.position = GameManager.Instance.mousePos;
                chosenSlash.Play();
                return;
            }
        }
        GameObject newobj = Instantiate(Slash_Prefab, Slash_Parent);
        Slash_List.Add(newobj.GetComponent<ParticleSystem>());
        chosenSlash = newobj.GetComponent<ParticleSystem>();
        chosenSlash.transform.position = GameManager.Instance.mousePos;
        chosenSlash.Play();
    }

    void StopSlash()
    {
        chosenSlash.Stop();
    }
}
